const axios = require('axios');
const https = require('https');
const builder = require('xmlbuilder');

const instance = axios.create({
    httpsAgent: new https.Agent({  
      rejectUnauthorized: false
    })
  });


module.exports = (app) => {
    return {
        checkIsCorrecao: async (commitsPassedInRegex,branchName) => {
            const log = app.services.log.get('external:check-is-correcao')
            log.info('Making promises of requisitions to skw')
            const arrProm = [];
            for(i = commitsPassedInRegex.length -1; i>=0; i-=1){
                const commitPassedInRegex = commitsPassedInRegex[i];
                log.debug(`Making Promise to commit ${commitPassedInRegex}`)
                arrProm[arrProm.length] = axios.post(`https://skw.company.com.br/slackcmd/os`, `token=${TOKEN}&user_name=${USER}&text=${commitPassedInRegex.NUMOS}`, { headers: {'content-type': 'application/x-www-form-urlencoded', 'cache-control': 'no-cache'} });
            }
            try {
                const results = await Promise.all(arrProm);
                log.info('All requests have success')
                for(i = results.length -1; i>=0; i-=1){
                    if (results[i].data.text.search('_Tipo: Correção de erro') == -1) {
                        if (!(String(branchName).includes("4.12"))) {
                            app.services.error(app.config.consts.ERROR_TYPE_OS_IS_NOT_CORRECTION + branchName);
                        }
                    }
                }
            } catch (err) {
                log.error(JSON.stringify(err))
            }
        },
        releaseSkHomolog: async (commitsPassedInRegex,branchName) => {
            const log = app.services.log.get('external:release-sk-homolog')

            var xmlCab = builder.create('autocmp-info')
                .att('token', '$TOKEN')
                .att('appName', '')
                .att('action', '')
                .att('compilacaoCodUsu', '')
                .att('compilacaoNomeUsu', '');
    
            log.info('Additioning OSs on xml')
            for (i = commitsPassedInRegex.length -1; i>=0; i-=1) {
                const commitPassedInRegex = commitsPassedInRegex[i];
                xmlCab.ele('numOs', commitPassedInRegex.NUMOS);
                log.debug(`OS ${commitPassedInRegex.NUMOS} added`)
            }
    
            var fullXml = xmlCab.end({pretty: true});
            log.info('Xml created with success')
            var config = {
                method: 'post',
                url: 'https://skw.company.com.br/autocmp/filaPendencias',
                headers: { 
                'Content-Type': 'application/xml',
                },
                data: fullXml
            };

            let logMsg = `Commits na versão ${branchName}\n`
                .concat('  Ordens de Serviço a serem encaminhadas:');

            for (i = commitsPassedInRegex.length -1; i>=0; i-=1) {
            logMsg = logMsg.concat('\n    ')
                .concat(commitsPassedInRegex[i].NUMOS);
            }

            log.notice(logMsg)
            
            await axios(config).then(function (response) {
                log.notice(app.config.consts.SUCCESS_RELEASED_SKHOMOLOG)
            }).catch(function (error) {
                log.error(error)
            });
        },
        releaseDbScripts: async (sendData) => {
            const log = app.services.log.get('external:release-db-scripts')

            const jenkinsurl = process.env.JENKINS_URL || '201.48.95.54'
            log.warning(`Skipping the script with jenkins on ${jenkinsurl} with the payload above \n ${JSON.stringify(sendData)}`)
            // const result = await instance.put(`https://${jenkinsurl}:80/v1/script`, sendData, { headers: {'authorization': 'b16727a3476d06f9108517e47f59f0d7'} })
            // if (result.status != 200){
            //     app.services.error(app.config.consts.ERROR_OFFCIALIZE_OS + sendData.NUMOS);
            // }
        }

    }
}
