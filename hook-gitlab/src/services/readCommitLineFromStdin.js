const readline = require('readline');

module.exports = (app) => new Promise((resolve) => {
  
  const reader = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
  })

  reader.on('line', function(line){
    resolve(line.split(" "));
  });
});