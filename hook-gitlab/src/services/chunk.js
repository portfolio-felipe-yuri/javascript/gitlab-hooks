const chunk = require('chunk');

module.exports = app => ({
  insertChunked: (rowsToBeInserted, insertFunction) => {
    const log = app.services.log.get('chunk:insert-chunked')
    log.debug(`Chunking ${rowsToBeInserted.length} rows to be inserted by the above function \n ${insertFunction}`)
    const chunksOfRowsToBeInserted = chunk(rowsToBeInserted, 500);
    log.debug(`${chunksOfRowsToBeInserted.length} chunks was created`)
    const arrProm = [];
    for (let i = chunksOfRowsToBeInserted.length - 1; i >= 0; i -= 1) {
      const rowsChunked = chunksOfRowsToBeInserted[i];
      arrProm[arrProm.length] = insertFunction(rowsChunked);
    }
    log.debug(`Returning a array with ${arrProm.length} promises`)
    return arrProm;
  },
});
