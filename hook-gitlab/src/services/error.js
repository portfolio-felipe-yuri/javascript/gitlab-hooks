module.exports = (app) => (message = "Erro desconhecido") => {
    console.log(`GL-HOOK-ERR: ${message}`);
    console.error(`GL-HOOK-ERR: ${message}`);

    process.exit(1);
}