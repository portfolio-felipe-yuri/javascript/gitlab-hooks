module.exports = (app) => {
    return (funcBypass, funcToCall) => {
        const log = app.services.log.get('bypasser')
        log.debug(`executing the function bypass ${funcBypass}`)
        if (!funcBypass()){
            log.debug('Bypass returned false')
            return funcToCall
        }
        log.debug('Bypass returned true')
        return (obj) => obj;
    }
}
