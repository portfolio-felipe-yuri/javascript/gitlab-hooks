module.exports = (app) => async (branchName) => {
    const versionsToBlock = ["4.15", "4.15-RC", "4.15-DEV"];

    if (versionsToBlock.includes(branchName)) {
        app.services.error(app.config.consts.ERROR_LOCKED_VERSION);
    }
}
