module.exports = (app) => function (obj){
    const log = app.services.log.get('dont-allow-new-branches')
    if(process.env.GL_USERNAME != 'branch.estavel'){
        log.debug(`the username isn't branch.estavel. testing local_sha`)
        if (obj.stdin.local_sha == '0000000000000000000000000000000000000000'){
            log.notice(`the user ${process.env.GL_USERNAME} can't create a new branch`)
            app.services.error(app.config.consts.ERROR_DONT_ALLOW_NEWBRANCH);
        }
    }
    return obj
}
