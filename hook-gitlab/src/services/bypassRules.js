const qaUsers = new RegExp("$USERS");
const usersToBypassOsImplementacao = new RegExp("(nobody)");
const regexCherryPick = new RegExp("cherry-pick-.{8}");
const usersByPassAll = new RegExp("$USERS");

module.exports = (app) => {
    return {
        canBypassOsImplementacao: function (){
            return usersToBypassOsImplementacao.test(process.env.GL_USERNAME) || qaUsers.test(process.env.GL_USERNAME)
        },
        bypassQA : function(messageToNoBypass) {
            if (!qaUsers.test(process.env.GL_USERNAME)){
                app.services.error(messageToNoBypass)
            }
        },
        bypassCherryPick: function(obj){
            if (regexCherryPick.test(obj.branchName)){
                const log = app.services.log.get('bypass-rules:bypass-cherry-pick')
                log.notice(`${obj.branchName} bypassed`)
                process.exit(0);
            }
            return obj
        },
        isUserByPassAll: () => {
            if (usersByPassAll.test(process.env.GL_USERNAME)){
                return true
            }
            return false
        }
    }
}
