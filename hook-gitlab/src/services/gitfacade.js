const { promisify } = require('util');
const exec = promisify(require('child_process').exec);
const commitRegex = new RegExp("OS:\\s+([0-9]+)");
const mergeRegex = /\b^(fix|feat|perf):\d+/;


module.exports = (app) => {
    const getChangeDetails = (changedFiles) => {
        const log = app.services.log.get('git-facade:get-change-details')
    
        const lines = changedFiles.split('\n');
        log.info(`${lines.length -1} files changed`)
        const modulesAndFiles = [];
        for (let i = lines.length - 1; i >= 0; i -= 1) {
          const line = lines[i];
          if (line !== '') {
            const [TYPE,PATH,UPDATE] = line.split('\t')
            const fonteSplited = PATH.split('/');
            const MODULO = fonteSplited[0];
            const FONTE = fonteSplited[fonteSplited.length - 1];
            const details = {
                FONTE,
                MODULO,
                PATH,
                UPDATE,
                TYPE
              };
            modulesAndFiles[modulesAndFiles.length] = details;
            log.debug(`the details above was added to the return array \n ${JSON.stringify(details)}`)
          }
        }
        return modulesAndFiles;
      };
    
    const getParents = (hash) => {
        return exec(`git cat-file -p ${hash} | grep parent`);
    }

    const normalizeCommitsInfo = (commit, BRANCHNAME, NUMOS = false) => {
        const commitObj = {
            MENSAGEM: commit.message,
            COMMITHASH: commit.hash,
            BRANCHNAME
        }
        if(NUMOS){
            commitObj.NUMOS = NUMOS
        }
        return commitObj
    }
    
    return {
        getParents,
        normalizeCommitsInfo,
        checkHashCommitInBranch: async (branchTarget, hashCommitList) => {
            const log = app.services.log.get('git-facade:check-hash-commit-in-branch')
            log.info(`Checking if branch ${branchTarget} have the right commits`)
            try {
                for (const hashCommit of hashCommitList) {
                    
                    log.debug(`Checking parents of commit ${hashCommit.COMMITHASH}`)
                    const parents = await getParents(hashCommit.COMMITHASH)
                    if (parents.stdout.trim().split('\n').length == 1) {
                        log.debug(`just one parent, now will check if the commit exist in target branch`)
                        const result = await exec(`git log --pretty=oneline ${branchTarget} | grep ${hashCommit.COMMITHASH} | awk '{print $1}'`);
            
                        if (result.stderr != "") {
                          app.services.error(app.config.consts.ERROR_VERIFY_HASH + branchTarget);
                        }
            
                        if (result.stdout == null || !result.stdout.trim()) {
                          app.services.error(`${app.config.consts.ERROR_COMMIT_DONT_EXISTS} ${branchTarget}`);
                        }
                    }else{
                        log.debug(`Skipping commit ${hashCommit.COMMITHASH} because have more than one parent`)
                    }
                }
    
            } catch (err) {
                app.services.error(app.config.consts.ERROR_VERIFY_HASH + branchTarget + `\n${err}`);
            }
        },
    
        getCommitsPushed: async (local_sha, remote_sha, branchName) => {
            const log = app.services.log.get('git-facade:get-commits-pushed')

            log.info(`Getting all commits between ${local_sha} and ${remote_sha}`)
            try {
                const commitList = await exec(`git rev-list --pretty=oneline ${local_sha}..${remote_sha}`)
                return commitList.stdout.split('\n').map(commitWithHash => {
                    return {
                        message: commitWithHash.substring(41),
                        hash: commitWithHash.slice(0,40)
                    }
                }).filter(arrElement => arrElement.hash != '')
                .map(commit => normalizeCommitsInfo(commit,branchName))
            } catch (err) {
                log.info('No commits found. Returning empty array')
                return [];
            }
        },
        getCommitsWhoPassInRegex: (commits, BRANCHNAME) => {
            const log = app.services.log.get('git-facade:get-commits-pushed')
            log.info(`Getting commit who matched with regex '${commitRegex}'`)
            return commits.map(commit => {
                const matches = commit.MENSAGEM.match(commitRegex) 
                
                if(matches){
                    log.debug(`Matched with the message: ${commit.MENSAGEM}`)
                    return {... commit, NUMOS: matches[1]}
                }else{
                    log.debug(`Don't matched with the message: ${commit.MENSAGEM}`)
                }
            }).filter(arrElement => arrElement != null)
    
        },
        fillCommitsWithAuthorNameAndDate: async (commitsPassedInRegex) => {
            const log = app.services.log.get('git-facade:fill-commits-with-author-name-and-date')

            for(let i = commitsPassedInRegex.length -1; i>= 0; i-=1){
                const commit = commitsPassedInRegex[i];
                commit.AUTOR = (await exec(`git show -s --format='%aN' ${commit.COMMITHASH}`)).stdout.replace('\n','')
                log.debug(`the author to the commit ${commit.COMMITHASH} is ${commit.AUTOR}`)
                commit.DATA = new Date();
            }
        },
        getModifiedFilesInCommitts: async (commitsPassedInRegex,branchName) => {
            const log = app.services.log.get('git-facade:get-modified-files-in-committs')

            const filesInCommits = [];
            for(let i = commitsPassedInRegex.length -1; i>= 0; i-=1){
                const commit = commitsPassedInRegex[i];
                log.info(`getting files changed to the commit ${commit.COMMITHASH}`)
                const changedFiles = (await exec(`git show --name-status --pretty='' ${commit.COMMITHASH}`,{
                    maxBuffer: 1024 * 1024 * 100 
                })).stdout

                const modulesAndFiles = getChangeDetails(changedFiles);

                for (let x = modulesAndFiles.length - 1; x >= 0; x -= 1) {
                    if (modulesAndFiles[x].FONTE && modulesAndFiles[x].MODULO) {
                      const fileDetail = {
                        ...modulesAndFiles[x],
                        VERSAO: branchName,
                        COMMITHASH: commit.COMMITHASH,
                      }
                      log.debug(`Added the fileDetail above to the filesInCommits \n ${JSON.stringify(fileDetail)}`)
                      filesInCommits[filesInCommits.length] = fileDetail;
                    }
                }
            }
            return filesInCommits
        },
        getFileContent: (remote_sha, filePath) => {
            return exec(`git show ${remote_sha}:${filePath}`);
        },
        filterCommitMergeParents: async (commit) => {
            const parents = await getParents(commit.COMMITHASH);
            if (parents.stdout.trim().split('\n').length == 1) {
                return null;
            }

            return commit;
        },
        isMergeWithCorrectMessage: (msg)=>{
            return mergeRegex.test(msg);
        }
    }
}
