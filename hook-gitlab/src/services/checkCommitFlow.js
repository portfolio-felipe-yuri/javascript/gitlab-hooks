module.exports = (app) => ({
  snkFlow: async (commitsPushed, branchName) => {
    const log = app.services.log.get('check-commit-flow:snkflow')
  
    const branchMatch = branchName.toUpperCase().match(/(?:^(\d\.\d{1,2})(?:-(DEV|RC))?)/);
  
    if (branchMatch && branchMatch[2] != "DEV" && (branchMatch[1] >= "4.8" || branchMatch[1] == "4.11" || branchMatch[1] == "4.12")) {
      let branchTarget;
  
      if (branchMatch[2] == "RC") {
          branchTarget = `${branchMatch[1]}-DEV`;
      } else {
        branchTarget = `${branchMatch[1]}-RC`;
      }
      log.info(`branch matched and the target is ${branchTarget}`)
      await app.services.gitfacade.checkHashCommitInBranch(branchTarget, commitsPushed);
    }
  },
  gitFlow: async (commitsPushed, branchName) => {
    const log = app.services.log.get('check-commit-flow:gitflow')
    
    let branchTarget = false;

    if(branchName == 'master'){
      branchTarget = 'release'
    }
    if(branchName == 'release'){
      branchTarget = 'develop'
    }
    if(branchTarget){
      log.info(`branch matched and the target is ${branchTarget}`)
      await app.services.gitfacade.checkHashCommitInBranch(branchTarget, commitsPushed);
    }
  }

})

