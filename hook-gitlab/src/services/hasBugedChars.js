const fs = require('fs');

const bugedChars = ['Ã', '', '', '', '', '', '', '', '', '', '', '', '', '¡', '©', '³', 'º', '£', 'µ', '¢', 'ª', '®', '´', '»', ' ', '¨', '¬', '²', '¹', '¼', '§'];

module.exports = (app) => async (remote_sha, modifiedFiles) => {
    const log = app.services.log.get('has-buged-chars')

    for (const modifiedFile of modifiedFiles) {
        log.info(`checking bugChars on file ${modifiedFile.PATH}`)
        let contentFile = await app.services.gitfacade.getFileContent(remote_sha, modifiedFile.PATH);

        if (contentFile.stderr != '') {
            throw contentFile.stderr;
        }

        bugedChars.forEach(bugedChar => {
            if (contentFile.stdout.includes(bugedChar)) {
                app.services.error(app.config.consts.ERROR_INVALID_CHARACTER + modifiedFile.PATH);
            }
        });
    }
}
