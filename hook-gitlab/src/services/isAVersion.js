const versionPattern = new RegExp("\\b^[0-9]\\.[0-9]{1,2}$");

module.exports = (app) => (remote_ref) =>{
    return versionPattern.test(remote_ref)
}