const knex = require('knex')
const autoservicecrud = require('@js-skeleton/autoservicecrud');

module.exports = (app) => {
    app.db = knex({
        client: 'mysql2',
        connection: {
          host: process.env.DB_HOST,
          user: process.env.DB_USER,
          password: process.env.DB_PASSWORD,
          database: process.env.DB_DATABASE,  
        },
      });
    
    
      const writeCommiOnVersions =  async (commitsPassedInRegex, modifiedFilesInCommitsProm) => {
        const log = app.services.log.get('write-commits-on-db:write-commit-on-versions')

        log.info(`Creating insertFunctions`)
        const {insert: insertFontes} = autoservicecrud(app, 'commitos_fontes');
        const {insert: insertCommitos} = autoservicecrud(app, 'commitos');
        const {save: saveUltimoCommitos} = autoservicecrud(app, 'ultimocommitos', [
            ['save', (cApp,table) => async ({filter,ultimoCommitos}) => {
                const result = await cApp.db(table).where(filter).select()
                if (result.length == 0){
                    return cApp.db(table).insert(ultimoCommitos)
                }
                return cApp.db(table).where(filter).update(ultimoCommitos)
                
            }]
        ]);
      
        log.info(`Functions created`)
      
        let insertChunks = app.services.chunk.insertChunked(commitsPassedInRegex,insertCommitos)
        log.debug(`Created ${insertChunks.length} chunks to insert on commitos`)
        let ultimoCommitos = {};
      
        for (const commit of commitsPassedInRegex) {
            const parents = await app.services.gitfacade.getParents(commit.COMMITHASH)
            
            if (parents.stdout.trim().split('\n').length > 1){
              continue
            }
      
            let tmpCommit = {
                NUMOS: commit.NUMOS,
                VERSAO: commit.BRANCHNAME,
                COMMITHASH: commit.COMMITHASH,
                DATA: commit.DATA
            };
              
              
            if(!ultimoCommitos[commit.NUMOS] || ultimoCommitos[commit.NUMOS].DATA < commit.DATA){
                ultimoCommitos[commit.NUMOS] = tmpCommit
                log.debug(`Added the object above on array of ultimoCommitos \n ${JSON.stringify(tmpCommit)}`)
            }
              
              
          }
        
        log.debug(`Awaiting modifiedFilesInCommitsProm`)
        const modifiedFilesInCommits = await modifiedFilesInCommitsProm
        log.debug(`Awaiting Promise.all(insertChunks)`)
        await Promise.all(insertChunks)
        log.info(`Inserts on commitos finished`)
        const insertsOfUltimoCommitos = [];
      
          for (const key in ultimoCommitos) {
              const commit = ultimoCommitos[key];
              log.debug(`pushing the commit object above to be inserted in ultimoCommitos \n ${JSON.stringify(commit)}`)
              insertsOfUltimoCommitos.push(saveUltimoCommitos({
                  filter:{NUMOS: commit.NUMOS, VERSAO: commit.VERSAO},
                  ultimoCommitos: commit
              })) 
              
          
          }
      
      
       
        insertChunks = app.services.chunk.insertChunked(modifiedFilesInCommits,insertFontes);
        log.debug(`Created ${insertChunks.length} chunks to insert on commitos_fontes`)
        await Promise.all(insertsOfUltimoCommitos);
        await Promise.all(insertChunks);
        }


    return {
        writeCommiOnVersions,
        closeDbConnection : () => app.db.destroy() ,
        writeDBScriptOnDB : async (branchName, modifiedFilesInCommitsProm) => {
            const log = app.services.log.get('write-commits-on-db:write-db-script-on-db')

            log.info(`Creating insertFunctions`)
            let insert;
            
            const MGEPES_PROJECT_ID = 1358;
            const dbRepoIds = [MGEPES_PROJECT_ID];
            const projectId = process.env.GL_REPOSITORY.split('-').pop();

            if(projectId.includes(dbRepoIds)){
                const projectPathSplited = process.env.GL_PROJECT_PATH.split('/')
                projectPathSplited.pop();
                const customDbSrcTableName = `dbsrcmodify_${projectPathSplited.join('_').toLowerCase()}`
                insert = (autoservicecrud(app, customDbSrcTableName)).insert;

            }else{
                insert = (autoservicecrud(app, 'dbsrcmodify')).insert;
            }
            log.info(`Functions created`)

            const modifiedFilesInCommits = await modifiedFilesInCommitsProm
            log.info(`Checking if have files to write on dbsrcmodify`)
            if(modifiedFilesInCommits && modifiedFilesInCommits.length > 0){

                const modifiedFilesInDbScript = modifiedFilesInCommits.filter(file => {
                    
                    if (file.PATH.search('dbscript') != -1 && file.TYPE != 'D'){
                        return true
                    }else{
                        log.debug(`file ${file.PATH} is not in dbscript folder or is a deletion`)
                    }
                    return false
                })
                
                const scriptsPreparedToInsert = modifiedFilesInDbScript.map(file => {
                    const currentPath = file.UPDATE == undefined ? file.PATH : file.UPDATE;

                    return {
                        path: currentPath,
                        branchName,
                        modifyTime: new Date(),
                        gitUser: process.env.GL_USERNAME
                    }
                })
        
                log.debug(`the object above will be inserted on dbsrcmodify \n ${JSON.stringify(scriptsPreparedToInsert)}`)
                await Promise.all(app.services.chunk.insertChunked(scriptsPreparedToInsert,insert))
                log.info('Rows writed in dbsrcmodify successfuly')
        
                return true
            }else{
                log.notice('No modified file to write on dbsrcmodify')
            }
    
        }
    }
}


