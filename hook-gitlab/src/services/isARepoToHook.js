module.exports = (app) => {
    return () => {
        const repoPathToCheck = new RegExp("$PROJETOS");
        const pathsToHook = [
            'group/company',
        ]
        const log = app.services.log.get('is-a-repo-to-hook')

        let isNotARepoToHook = true; 
        log.info(`Checking if `)
        pathsToHook.forEach(pathToHook => {
            if(process.env.GL_PROJECT_PATH.startsWith(pathToHook)){
                isNotARepoToHook = false;
            }
        })

        if(isNotARepoToHook == true){
            log.info(`${process.env.GL_PROJECT_PATH} is not a repo to hook, exiting process !`)
            process.exit(0);
        }

        log.info(`${process.env.GL_PROJECT_PATH} Is a repo to hook`)
    }
}