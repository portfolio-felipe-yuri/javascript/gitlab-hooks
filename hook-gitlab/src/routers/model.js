module.exports = (app) => {
    return () => {
        const log = app.routers.log.get('ear-pessoal-mgepesmodel')
        const bypasser = app.services.bypasser;
        const {isUserByPassAll} = app.services.bypassRules
        log.info('creating chains')
        const chainer = new app.controllers.chainer()
        chainer.use(bypasser(isUserByPassAll,app.controllers.branches.verifyBranchName))
        chainer.use(bypasser(isUserByPassAll,app.controllers.commits.verifyGitCorrectFlow))
        chainer.use(bypasser(isUserByPassAll,app.controllers.commits.verifyCorrectMergeMessage))
        chainer.use(app.controllers.persistent.saveChangesOnMicroDbSrcModify)

        return chainer
    }
}