module.exports = (app) => {
    return () => {
        const log = app.routers.log.get('group-projetox')
        log.info('creating chains')
        const chainer = new app.controllers.chainer()
        chainer.use(app.controllers.commits.verifySnkCorrectFlow)
        return chainer
    }
}