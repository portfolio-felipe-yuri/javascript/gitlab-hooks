module.exports = (app) => {
    return () => {
        const log = app.routers.log.get('plataformaw-companyw:validate-commit')
        const bypasser = app.services.bypasser;
        const {isUserByPassAll} = app.services.bypassRules

        log.info('creating chains')
        const chainer = new app.controllers.chainer()
        chainer.use(bypasser(isUserByPassAll,app.services.dontAllowNewBranches))
        chainer.use(bypasser(isUserByPassAll,app.controllers.commits.isOnlyQaCommit)) // bypassAll
        //chainer.use(bypasser(isUserByPassAll,app.controllers.commits.isBlockBranch)) // bypassAll
        chainer.use(bypasser(isUserByPassAll,app.controllers.commits.dontAllowOsImplementacao)) // bypassall
        chainer.use(app.controllers.commits.verifySnkCorrectFlow)
        chainer.use(app.controllers.commits.skHomolog)
        chainer.use(app.controllers.persistent.saveChangesOnDbs)

        return chainer 
    }
}
