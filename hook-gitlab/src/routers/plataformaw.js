module.exports = (app) => {
    return () => {
        const log = app.routers.log.get('group')
        log.info('creating chains')
        const chainer = new app.controllers.chainer()
        const bypasser = app.services.bypasser;
        const {isUserByPassAll} = app.services.bypassRules

        chainer.use(bypasser(isUserByPassAll,app.controllers.commits.needAtLeastOneCommitPassedInRegex))
        return chainer
    }
}