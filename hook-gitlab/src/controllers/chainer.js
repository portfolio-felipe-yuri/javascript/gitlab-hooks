
module.exports = (app) => {
    class Chainer 
    { 
        constructor(){
            this.log = app.controllers.log.get('chainer');
            this.functionsToCall = [];
        }
       use(functionToCall){
           this.functionsToCall.push(functionToCall)
        }
       router(repository,routerChainer){
           this.log.debug(`Tryng to add route to repository ${repository}`)
           if (process.env.GL_PROJECT_PATH.startsWith(repository)){
               this.log.info(`Added route to ${repository}`)
               this.use(routerChainer)
           }
       }
       async execute(retObj){
           
           const numOfChain =  this.functionsToCall.length
           for(let i = 0; i<numOfChain; i++){
               if(!retObj){
                   retObj = {}
               }
               const functionToCall = this.functionsToCall[i] 
    
                if(!functionToCall.execute){
                    this.log.debug(`executing function \n ${functionToCall} \n with the param \n ${JSON.stringify(retObj)}`)
                    retObj = await functionToCall(retObj)
                }else{
                    this.log.debug(`Jumping to functions of a router with the param \n ${JSON.stringify(retObj)}`)                
                    retObj = await functionToCall.execute(retObj)
                }
    
            }
    
            return retObj
       }
    
    } 

    return Chainer
}
