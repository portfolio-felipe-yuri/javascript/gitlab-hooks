module.exports = (app) => {    
    return {
        getCommitsInfos: async (obj) => {
            const log = app.controllers.log.get('commits:get-commits-infos')
            log.debug('Starting to get commit infos')
            const commitsPushed = await  app.services.gitfacade.getCommitsPushed(obj.stdin.local_sha,obj.stdin.remote_sha,obj.branchName)
            const commitsPassedInRegex = app.services.gitfacade.getCommitsWhoPassInRegex(commitsPushed,obj.branchName)
            const commitsWithAuthorNameProm = app.services.gitfacade.fillCommitsWithAuthorNameAndDate(commitsPassedInRegex)
            const modifiedFilesInCommitsRegexProm = app.services.gitfacade.getModifiedFilesInCommitts(commitsPassedInRegex,obj.branchName)
            const modifiedFilesInCommitsPushedProm = app.services.gitfacade.getModifiedFilesInCommitts(commitsPushed,obj.branchName)
            obj.commits = {
                commitsPassedInRegex,
                commitsPushed,
                commitsWithAuthorName: await commitsWithAuthorNameProm,
                modifiedFilesInCommitsRegex: await modifiedFilesInCommitsRegexProm,
                modifiedFilesInCommitsPushed: await modifiedFilesInCommitsPushedProm
            }
            log.info(`Informations fetched from commits \n ${JSON.stringify(obj.commits)}`)
            return obj
        },
        needAtLeastOneCommitPassedInRegex : (obj) => {
            if (obj.commits.commitsPassedInRegex.length === 0 && obj.commits.commitsPushed.length !== 0){
                app.services.error(app.config.consts.ERROR_COMMIT_WITHOUT_OS);
            }
            return obj
        },
        isOnlyQaCommit: (obj) => {
            const log = app.controllers.log.get('commits:is-only-qa-commit')
            if (!(obj.branchName.includes("-DEV")) && obj.branchName.length < 8) {
                log.notice(`Checking if user ${process.env.GL_USERNAME} is in the bypassQA`)
                app.services.bypassRules.bypassQA(app.config.consts.ERROR_CONTACT_QA_TO_APRROVE)
            }
            return obj
        },
        dontAllowOsImplementacao: async (obj) => {
            if(obj.branchName != '4.16-DEV'){
                const log = app.controllers.log.get('commits:is-only-qa-commit')
    
                const dontAllowOSImplBranches = /\b^\d\.\d{1,2}-DEV$/g
                if(dontAllowOSImplBranches.test(obj.branchName) && !app.services.bypassRules.canBypassOsImplementacao()){
                    log.notice(`the branch ${obj.branchName} don't allow implementations and ${process.env.GL_USERNAME} can't bypass it`)
                    await app.services.external.checkIsCorrecao(obj.commits.commitsPassedInRegex,obj.branchName)
                }
            }
            
            
            return obj
        },
        verifySnkCorrectFlow: async (obj) => {
            if (obj.branchName != "4.12.2") {
                await app.services.checkCommitFlow.snkFlow(obj.commits.commitsPushed,obj.branchName)
            }
            return obj

        },
        verifyGitCorrectFlow: async (obj) => {
            await app.services.checkCommitFlow.gitFlow(obj.commits.commitsPushed,obj.branchName)
            return obj
        },
        skHomolog: async (obj) => {
            if(app.services.isAVersion(obj.branchName)){
                await app.services.external.releaseSkHomolog(obj.commits.commitsPassedInRegex,obj.branchName)
            }
            return obj
        },
        verifyBugedChars: async (obj) => {
            await app.services.hasBugedChars(obj.stdin.remote_sha,obj.commits.modifiedFilesInCommitsPushed)
            return obj
        },
        isBlockBranch: async (obj) => {
            await app.services.blockCommit(obj.branchName)
            return obj
        },
        verifyCorrectMergeMessage: async (obj) => {
            const log = app.controllers.log.get('commits:verify-correct-merge-message')
            const closedBranchRegex = /\b^(master|develop|release)$/;

            if(!obj.branchName.match(closedBranchRegex)){
                log.info(`${obj.branchName} is a developer branch, skipping the verify`)
                return obj
            }
            const promOfResults = []
            obj.commits.commitsPushed.forEach((commit) => {
                 promOfResults.push(app.services.gitfacade.filterCommitMergeParents(commit))
            })

            var results = await Promise.all(promOfResults);
            results = results.filter(value => value != null); 

            if(results.length == 0){
                log.debug(`There is no merge for branch to be evaluated`)
                return obj;
            }

            const msgCorrect = []
            results.forEach(commit => {
                msgCorrect.push(app.services.gitfacade.isMergeWithCorrectMessage(commit.MENSAGEM))
            });

            log.debug(`The result array to check if the commmit is a merge with correct message is above \n ${JSON.stringify(msgCorrect)}`)
            if(!msgCorrect.includes(true)){
                app.services.error(app.config.consts.ERROR_MERGE_NO_PATTERN);
            }
            
            return obj
        }
    }
}
