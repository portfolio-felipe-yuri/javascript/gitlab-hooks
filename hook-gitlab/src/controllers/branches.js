module.exports = (app) => {    
    return {
        verifyBranchName: async (obj) => {
            const log = app.controllers.log.get('branches:verify-branch-name')

            const closedBranchRegex = /\b^(master|develop|release)$/;
            const branchRegex = /(hot|hotfix|bug|bugfix|feat)-(\d+)?((ga|rc|dev|([0-9]+\.[0-9]+\.x))-([\w-]+))?/;
            log.debug(`Checking if branchName matches with the pattern ${branchRegex} or ${closedBranchRegex}`)
            if (!obj.branchName.match(branchRegex) && !obj.branchName.match(closedBranchRegex)){
                app.services.error(app.config.consts.ERROR_WRONG_BRANCHNAME)
            }          
            return obj
        }
    }
}
