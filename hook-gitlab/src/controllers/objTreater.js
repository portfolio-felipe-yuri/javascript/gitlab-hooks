module.exports = (app) => {
    return {
        readLineFromStdin: async (obj) => {
            const log = app.controllers.log.get('obj-treater:read-line-from-stdin')
            log.debug('awaiting the readline')
            const [local_sha, remote_sha, remote_ref ] = await app.services.readCommitLineFromStdin;
            
            obj.stdin = {
                local_sha,
                remote_sha,
                remote_ref
            }
            log.debug('Line readed, getting branch name')
            obj.branchName = remote_ref.split("refs/heads/")[1];
            log.info(`the branch name is ${obj.branchName} and the stdin is \n ${JSON.stringify(obj.stdin)}`)
            return obj
        }
    }
}