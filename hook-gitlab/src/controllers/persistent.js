module.exports = (app) => {
    return {
        saveChangesOnDbs: async (obj) => {
            const log = app.controllers.log.get('persistent:save-changes-on-dbs')
            const arrOfWritesInDbProm = [];
            const { branchName } = obj
            log.info(`making a promise of writeDBScriptOnDB for branch ${branchName}`)
            arrOfWritesInDbProm.push(app.services.writeCommitsOnDB.writeDBScriptOnDB(branchName, obj.commits.modifiedFilesInCommitsPushed))
            if (app.services.isAVersion(branchName)) {
                const { commitsPassedInRegex } = obj.commits
                log.info(`The branch is a version, making a promise of writeCommiOnVersions`)
                arrOfWritesInDbProm.push(app.services.writeCommitsOnDB.writeCommiOnVersions(commitsPassedInRegex, obj.commits.modifiedFilesInCommitsRegex))
                log.info(`calling releaseDbScripts for ${commitsPassedInRegex.length} commits`)
                for (i = commitsPassedInRegex.length - 1; i >= 0; i -= 1) {
                    const { NUMOS, MENSAGEM } = commitsPassedInRegex[i];
                    const sendData = {
                        NUMOS: NUMOS,
                        commitMessage: MENSAGEM,
                        branchName
                    }
                    log.debug(`calling releaseDbScripts with the object above \n ${JSON.stringify(sendData)}`)
                    await app.services.external.releaseDbScripts(sendData)
                }

            }
            try {
                await Promise.all(arrOfWritesInDbProm)
            } catch (error) {
                log.notice(`Erro ao escrever commits no banco de dados, segue a baixo o objeto de erro \n ${JSON.stringify(error)}`)
            }
            log.info(arrOfWritesInDbProm.length == 2 ? 'Finished promises of writeDBScriptOnDB and writeCommiOnVersions' : 'Finished promises of writeDBScriptOnDB')
            return obj
        },

        saveChangesOnCommitOS: async (obj) => {
            if (app.services.isAVersion(obj.branchName)) {
                const prefixedFilesInCommitsRegex = obj.commits.modifiedFilesInCommitsRegex.map(modifiedFile => {
                    return { ...modifiedFile, PATH: `company-js/${modifiedFile.PATH}` }
                })
                await app.services.writeCommitsOnDB.writeCommiOnVersions(obj.commits.commitsPassedInRegex, prefixedFilesInCommitsRegex)
            }

            return obj
        },
        saveChangesOnMicroDbSrcModify: async (obj) => {
            try {
                await app.services.writeCommitsOnDB.writeDBScriptOnDB(obj.branchName, obj.commits.modifiedFilesInCommitsPushed)
            } catch (error) {
                log.notice(`Erro ao escrever commits no banco de dados, segue a baixo o objeto de erro \n ${JSON.stringify(error)}`)
            }
            return obj
        }
    }
}