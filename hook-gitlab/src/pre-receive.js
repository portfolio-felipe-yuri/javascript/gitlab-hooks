#!/usr/bin/env node
require('./config/dotenv')()

const userEnabledebug = ['igor.silva', 
                         'felipe.yuri',
                         'leonardo.mendes',
                         'jose.tobias'
                        ];

if(userEnabledebug.includes(process.env.GL_USERNAME)){
  process.env.LOG_LEVEL = 'debug'
}

const createLogger = require('./config/createLogger')

const consign = require('consign');
const app = {}

const consignConf = {
  cwd: process.env.testCWD ? `${process.env.testCWD}src` : '/data/hook-gitlab/src',
  // verbose: process.env.LOG_LEVEL == 'debug' ? true : false
  verbose: false
}

consign(consignConf)
  .include('./config')
  .then('./controllers')
  .then('./routers')
  .then('./services')
  .then('./routers') 
  .exclude('./config/dotenv.js')
  .exclude('./config/createLogger.js')
  .into(app);

createLogger(app);


app.log.info(`Working with the env var above \n ${JSON.stringify(process.env,null,2)}`)



app.log.info('Loading bonder...');
app.config.bonder()
app.log.info('bonder loaded ! executing chainer ');
app.chainer.execute().then( async () => {  
    app.log.notice('Push successful.')    
    process.exit(0);
  }).catch( async err => {
    app.log.error(err)    
    process.exit(1);
});
