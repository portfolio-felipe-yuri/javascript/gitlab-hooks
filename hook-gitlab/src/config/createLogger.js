process.env.FILE_TO_WRITE_LOG = process.env.FOLDER_TO_WRITE_LOG.concat(new Date().toISOString().substring(0, 10).concat('.log'));

require('@js-skeleton/log-node')();
const log = require('log');

module.exports = (app) => {
    const contexts = ['config','controllers','routers','services']
    app.log = log
    contexts.forEach((context) => {
        app[context].log = log.get(context)
    })
}
