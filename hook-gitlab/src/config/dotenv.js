
module.exports = () => {
    const path = process.env.testCWD || '/data/hook-gitlab/'
    let result = require('dotenv').config({path: `${path}.env`});
    if (result.error){
        result = require('dotenv').config();
        if (result.error){
            console.log("GL-HOOK-ERR: Falha ao carregar váriaveis de ambiente, contate a equipe de devops")
            process.exit(1)
        }
    }
}