module.exports = {
    ERROR_UNKNOWN: 'Erro desconhecido!',
    ERROR_DONT_ALLOW_NEWBRANCH: 'Não é permitido a criação de branches locais. Acesse: https://git.company.com.br/plataforma-w/branch-estavel/pipelines/new',
    ERROR_VERIFY_HASH: 'Ocorreu um erro ao tentar verificar o hash do commit na branch ',
    ERROR_COMMIT_DONT_EXISTS: 'O commit não existe na branch',
    ERROR_INVALID_CHARACTER: 'Caracteres inválidos encontrados no arquivo ',
    ERROR_MR_DONT_APPROVE: 'Apenas MR aprovados (botão azul <Approve>) pela equipe de QA podem ser mergeados localmente. Entre em contato com a equipe de QA.',
    ERROR_CONTACT_QA_TO_APRROVE: 'Entre em contato com a equipe de QA para autorizar esse merge.',
    ERROR_COMMIT_WITHOUT_OS: 'Mensagem de commit não possui um número de OS válido: \'OS: 123456 comentário\'',
    ERROR_TYPE_OS_IS_NOT_CORRECTION: 'A sua OS não é do tipo correção e por enquanto não pode ser commitada na branch ',
    ERROR_OFFCIALIZE_OS: 'Erro ao oficializar o script (de base de dados) da OS: ',
    ERROR_LOCKED_VERSION: 'Versão encontra-se bloqueada para commits!',
    SUCCESS_RELEASED_SKHOMOLOG: 'Sucesso ao liberar as OSs da fila skhomolog',
    ERROR_WRONG_BRANCHNAME: 'O nome da sua branch deve seguir o padrão \"/(hot|hotfix|bug|bugfix|feat)-(\d+)?((ga|rc|dev|([0-9]+\.[0-9]+\.x))-([\w-]+))?/\"',
    ERROR_MERGE_NO_PATTERN: 'O commit de merge deve seguir o padrão (fix\|feat\|break):999999'
  }
