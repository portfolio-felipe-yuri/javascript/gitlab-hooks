module.exports = (app) => {
    return () => {
        const chainer = new app.controllers.chainer()
        const log = app.config.log.get('bonder')

        log.info('Creating chains')
        chainer.use(app.services.isARepoToHook)
        chainer.use(app.controllers.objTreater.readLineFromStdin)
        chainer.use(app.services.bypassRules.bypassCherryPick)
        chainer.use(app.controllers.commits.getCommitsInfos)        
        

        chainer.router('plataforma-w', app.routers.plataformaw())

        chainer.router('plataforma-w/companyw', app.routers.plataformawcompanyw())
        chainer.router('plataforma-w/company-js', app.routers.plataformawcompanyjs())
        
        chainer.router('model-test', app.routers.erpPessoalMgepesmodel())
        chainer.router('erp/pessoal', app.routers.erpPessoalMgepesmodel())

        chainer.router('plataforma-w/fiscal/s', app.routers.plataformawFiscalSanNFe())
        
        chainer.use(app.services.writeCommitsOnDB.closeDbConnection)
        log.info('Chains created')
        app.chainer = chainer
    }
}
