var child_process = require('child_process');
const knex = require('knex');
require('dotenv').config({path: '/data/hook-gitlab/.env'});
const autoservicecrud = require('@js-skeleton/autoservicecrud');
const {prepareBeforeAll} = require('./utils')

const execOptions = {
    cwd: '/tmp/gitmock'
}



beforeAll(() => {
    prepareBeforeAll()
   
})

afterAll(() => {
    child_process.execSync(`rm -Rf ./gitmock-*`).toString();
})

beforeEach(async () => {
    execOptions.cwd = `${execOptions.cwd}-${Date.now()}`

    child_process.execSync(`mkdir -p ${execOptions.cwd}`).toString();
    child_process.execSync('git init',execOptions).toString();
    child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
    child_process.execSync('git add . && git commit -m "OS: 999999 teste"',execOptions).toString();
    firstCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
});

afterEach(async () => {
    execOptions.cwd = '/tmp/gitmock'
});

const makeDummyCommit = () => {
    child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
    child_process.execSync(`git add . && git commit -m "OS: 1697591 teste"`,execOptions).toString();
}

const spreadToBranches = (branch) => {
    try {
        child_process.execSync(`git branch -D fix-${branch}`,execOptions).toString();
        child_process.execSync(`git branch -D feat-${branch}`,execOptions).toString();
    } catch (err) {
        
    }
    
    child_process.execSync(`git checkout -b fix-${branch}`,execOptions).toString();
    child_process.execSync(`git checkout -b feat-${branch}`,execOptions).toString();
}



test('Deve permitir branchName no padrão (hot|bug|feat)-999999', async () => {
    makeDummyCommit()
    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    branches = ['hot','bug','feat']
    branches.forEach(branch => {
        try {
            const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'erp/pessoal/mge-pes-model',
                    GL_USERNAME: 'thaise.oliveira'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}-999999`.replace(/\n/g,'')
            }).toString();  
            expect(stdout).toMatch(/Push successful./);
        } catch (err) {
            console.log(err.stdout.toString())    
            expect('in a sucess will note execute this line').toBe('not executed');
        }
    })
});

test('Não deve permitir branchName fora do padrão (hot|bug|feat)-999999', async () => {
    makeDummyCommit()
    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();

    try {
        const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
            env: {... process.env,
                GL_PROJECT_PATH: 'erp/pessoal/mge-pes-model',
                GL_USERNAME: 'thaise.oliveira'
            },
            input: `${firstCommitHash} ${actualCommitHash} refs/heads/999999-4.12-DEV`.replace(/\n/g,'')
        }).toString();  
        expect('in a sucess will note execute this line').toBe('not executed');
    } catch (err) {
        expect(err.stdout.toString()).toMatch(/O nome da sua branch deve seguir o padrão/);    
    }
   
});

test('deve permitir commits de merge no padrão fix:999999 mensagem', async () => {
    const os = '999999'
    spreadToBranches(os);
    makeDummyCommit();
    child_process.execSync(`git checkout master`,execOptions).toString();
    child_process.execSync(`git merge --no-ff -m "fix:${os} teste" feat-${os}`,execOptions).toString();
    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();

    try {
        const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
            env: {... process.env,
                GL_PROJECT_PATH: 'erp/pessoal/mge-pes-model',
                GL_USERNAME: 'thaise.oliveira'
            },
            input: `${firstCommitHash} ${actualCommitHash} refs/heads/develop`.replace(/\n/g,'')
        }).toString();  
        expect(stdout).toMatch(/Push successful./);
    } catch (err) {
        console.log(err.stdout.toString())    
        expect('in a sucess will note execute this line').toBe('not executed');
    }

});

test('deve permitir commits de merge no padrão feat:999999 mensagem', async () => {
    const os = '999999'
    spreadToBranches(os);
    makeDummyCommit();
    child_process.execSync(`git checkout master`,execOptions).toString();
    child_process.execSync(`git merge --no-ff -m "feat:${os} teste" feat-${os}`,execOptions).toString();
    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();

    try {
        const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
            env: {... process.env,
                GL_PROJECT_PATH: 'erp/pessoal/mge-pes-model',
                GL_USERNAME: 'thaise.oliveira'
            },
            input: `${firstCommitHash} ${actualCommitHash} refs/heads/develop`.replace(/\n/g,'')
        }).toString();  
        expect(stdout).toMatch(/Push successful./);
        
    } catch (err) {
        console.log(err.stdout.toString())   
        expect('in a sucess will note execute this line').toBe('not executed');
 
    }

});

test('deve permitir commits de merge no padrão perf:999999 mensagem', async () => {
    const os = '999999'
    spreadToBranches(os);
    makeDummyCommit();
    child_process.execSync(`git checkout master`,execOptions).toString();
    child_process.execSync(`git merge --no-ff -m "perf:${os} teste" feat-${os}`,execOptions).toString();
    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();

    try {
        const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
            env: {... process.env,
                GL_PROJECT_PATH: 'erp/pessoal/mge-pes-model',
                GL_USERNAME: 'thaise.oliveira'
            },
            input: `${firstCommitHash} ${actualCommitHash} refs/heads/develop`.replace(/\n/g,'')
        }).toString();  
        expect(stdout).toMatch(/Push successful./);
    } catch (err) {
        console.log(err.stdout.toString())    
        expect('in a sucess will note execute this line').toBe('not executed');

    }

});

test('não deve permitir commits de merge fora padrão (fix|feat|break):999999 mensagem', async () => {
    const os = '999999'
    spreadToBranches(os);
    makeDummyCommit();
    child_process.execSync(`git checkout master`,execOptions).toString();
    child_process.execSync(`git merge --no-ff -m "batata:${os} teste" feat-${os}`,execOptions).toString();
    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();

    try {
        const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
            env: {... process.env,
                GL_PROJECT_PATH: 'erp/pessoal/mge-pes-model',
                GL_USERNAME: 'thaise.oliveira'
            },
            input: `${firstCommitHash} ${actualCommitHash} refs/heads/develop`.replace(/\n/g,'')
        }).toString();  
        expect('notExecute').toBe('In a success this line will never be executed');
    } catch (err) {
        expect(err.stdout.toString()).toMatch('O commit de merge deve seguir o padrão (fix|feat|break):999999')    
    }
});

describe('mock de branches no git', () => { 
    beforeEach(() => {
        child_process.execSync('git checkout -b develop',execOptions);
        child_process.execSync('git checkout -b release',execOptions);
        child_process.execSync('git checkout master',execOptions);

    });

    afterEach(() => {
        child_process.execSync('git branch -D develop',execOptions);
        child_process.execSync('git branch -D release',execOptions);
    })


    test('não deve permitir commits em branches release sem commit na develop no erp ', async () => {
        child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
        child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
        const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
        

        try{
            const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'erp/pessoal/mge-pes-model',
                    GL_USERNAME: 'thaise.oliveira'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/release`.replace(/\n/g,'')
            }).toString();   

            expect('in a sucess will note execute this line').toBe('not executed');
        } catch (err) {
            expect(err.stdout.toString()).toMatch(/O commit não existe na branch develop/);
        }
    
    });
    test('não deve permitir commits em branches master sem commit na release no erp ', async () => {
        child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
        child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
        const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
        

        try{
            const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'erp/pessoal/mge-pes-model',
                    GL_USERNAME: 'thaise.oliveira'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/master`.replace(/\n/g,'')
            }).toString();   

            expect('in a sucess will note execute this line').toBe('not executed');
        } catch (err) {
            expect(err.stdout.toString()).toMatch(/O commit não existe na branch release/);
        }
    
    });
 });