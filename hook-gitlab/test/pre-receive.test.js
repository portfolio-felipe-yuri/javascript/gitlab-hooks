var child_process = require('child_process');
const {prepareBeforeAll} = require('./utils')

beforeAll(() => {
    prepareBeforeAll()
})


test('Deve dar exit 0 quando não está em um repo para o hook', async () => {
    const stdout = child_process.execSync('node src/pre-receive.js', {
        env: {... process.env, LOG_LEVEL:'debug',
        GL_PROJECT_PATH: 'igorpdasilvaa/no-project'
        }}).toString();

    expect(stdout).toMatch(/is not a repo to hook, exiting process !/);
});

test('Deve dar exit 0 com branch do tipo cherry-pick', async () => {
    const stdout = child_process.execSync('node src/pre-receive.js', {
        env: {... process.env,
            GL_PROJECT_PATH: 'plataforma-w/companyw'
        },
        input: 'badhabsjhasdjhvb kksjdhbfjsfbsjdhf refs/heads/cherry-pick-asd5d496'
    }).toString();

    expect(stdout).toMatch(/cherry-pick-asd5d496 bypassed/);
});

test('Deve dar exit 1 com branch criação de branch local', async () => {
    try {
        const stdout = child_process.execSync('node src/pre-receive.js', {
            env: {... process.env,
                GL_PROJECT_PATH: 'plataforma-w/companyw'
            },
            input: '0000000000000000000000000000000000000000 926709e5e2bd33b2be388e285fe0c7bdcf366c04 refs/heads/minha-nova-branch'
        }).toString();
        expect('in a sucess will note execute this line').toBe('not executed');
    } catch (err) {
        expect(Buffer(err.stdout).toString()).toMatch(/GL-HOOK-ERR: Não é permitido a criação de branches locais/);
        expect(Buffer(err.stderr).toString()).toMatch(/GL-HOOK-ERR: Não é permitido a criação de branches locais/);
    }

});

test('Não deve permitir commits em branches que apenas o QA pode commitar', async () => {
    const branchesOnlyQaCommit = ['4.8','4.8-RC'];
    branchesOnlyQaCommit.forEach(branch => {
        try {
            const stdout = child_process.execSync('node src/pre-receive.js', {
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw'
                },
                input: `926709e5e2bd33b2be388e285fe0c7bdcf366c04 926709e5e2bd33b2be388e285fe0c7bdcf366c04 refs/heads/${branch}`
            }).toString();
        } catch (err) {
            expect(Buffer(err.stdout).toString()).toMatch(/GL-HOOK-ERR: Entre em contato com a equipe de QA para autorizar esse merge/);
            expect(Buffer(err.stderr).toString()).toMatch(/GL-HOOK-ERR: Entre em contato com a equipe de QA para autorizar esse merge/);
        }
    })

});

describe('mock de repositório git', () => {
    
    const execOptions = {
        cwd: '/tmp/gitmock'
    }

    let firstCommitHash = null;
    beforeEach(() => {
        child_process.execSync('mkdir -p /tmp/gitmock').toString();
        child_process.execSync('git init',execOptions).toString();
        child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
        child_process.execSync('git add . && git commit -m "OS: 999999 teste"',execOptions).toString();
        firstCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    });
    
    afterEach(() => {
        child_process.execSync('rm -Rf /tmp/gitmock').toString();
    });

    test('Não deve permitir commit com mensagem fora do padrão OS: 999999 mensagem', async () => {
        child_process.execSync('echo "teste" >> teste.txt',execOptions).toString();
        child_process.execSync('git add . && git commit -m "O teste"',execOptions).toString();
        const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();

        try {
            const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/123456-4.8-DEV`.replace(/\n/g,'')
            }).toString();

            expect('in a sucess will note execute this line').toBe('not executed');
            
        } catch (err) {
            expect(Buffer(err.stdout).toString()).toMatch(/GL-HOOK-ERR: Mensagem de commit não possui um número de OS válido: 'OS: 123456 comentário'/);
        }
        
    });
    
    test('deve permitir commit com mensagem dentro do padrão OS: 999999 mensagem', async () => {
        child_process.execSync('echo "teste" >> teste.txt',execOptions).toString();
        child_process.execSync('git add . && git commit -m "OS: 999999 teste"',execOptions).toString();
        const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
        
        
        const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
            env: {... process.env,
                GL_PROJECT_PATH: 'plataforma-w/companyw'
            },
            input: `${firstCommitHash} ${actualCommitHash} refs/heads/123456-4.8-DEV`.replace(/\n/g,'')
        }).toString();

        expect(stdout).toMatch(/Push successful/);  
        
    });

    test('deve permitir commits push de varios commmits, com 1 no padrão e 2 fora do padrão', async () => {
        child_process.execSync('echo "teste" >> teste.txt',execOptions).toString();
        child_process.execSync('git add . && git commit -m "OS: teste"',execOptions).toString();
        child_process.execSync('echo "teste" >> teste.txt',execOptions).toString();
        child_process.execSync('git add . && git commit -m "OS: 999999 teste"',execOptions).toString();
        child_process.execSync('echo "teste" >> teste.txt',execOptions).toString();
        child_process.execSync('git add . && git commit -m "OS: teste"',execOptions).toString();
        const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
        
        try {
            const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/123456-4.8-DEV`.replace(/\n/g,'')
            }).toString();
            
            expect(stdout).toMatch(/Push successful/);  
            
        } catch (error) {
            console.log(error.stdout.toString());
            throw error
        }
        
    });

    test('deve permitir commits do tipo implementação em branches 4.8-DEV para QA (canBypassOsImplementacao)', async () => {
        child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
        child_process.execSync('git add . && git commit -m "OS: 999999 teste"',execOptions).toString();
        const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
        
        const branchesNotPermmitFixes = ['4.8-DEV'];
        branchesNotPermmitFixes.forEach(branch => {
            try {
                const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                    env: {... process.env,
                        GL_PROJECT_PATH: 'plataforma-w/companyw',
                        GL_USERNAME: 'thaise.oliveira'
                    },
                    input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                }).toString();   
                expect(stdout).toMatch(/Push successful/); 
            } catch (error) {
                console.log(error.stdout.toString());
                throw error
            }
            
            
        })
    });

    test('não deve permitir commits do tipo implementação em branches 4.8-DEV para QA (canBypassOsImplementacao)', async () => {
        child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
        child_process.execSync('git add . && git commit -m "OS: 999999 teste"',execOptions).toString();
        const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
        
        const branchesNotPermmitFixes = ['4.8-DEV'];
        branchesNotPermmitFixes.forEach(branch => {
            try {
                const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                    env: {... process.env,
                        GL_PROJECT_PATH: 'plataforma-w/companyw'
                    },
                    input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                }).toString();   
                expect('in a sucess will note execute this line').toBe('not executed');
            } catch (error) {
                expect(Buffer(error.stdout).toString()).toMatch(/A sua OS não é do tipo correção e por enquanto não pode ser commitada na branch/);
            }
            
            
        })
    });
    
    test('deve permitir commits do tipo correção em branches 4.8-DEV ', async () => {
        child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
        child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
        const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
        

        const branchesNotPermmitFixes = ['4.8-DEV'];
        try {
            branchesNotPermmitFixes.forEach(branch => {
                const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                    env: {... process.env,
                        GL_PROJECT_PATH: 'plataforma-w/companyw'
                    },
                    input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                }).toString();   
    
                expect(stdout).toMatch(/Push successful/); 
                
                
            })            
        } catch (error) {
            console.log(error.stdout.toString());
            throw error
        }
    });

    describe('mock de branches no git', () => {   
        
        beforeEach(() => {
            child_process.execSync('git checkout -b 4.8-DEV',execOptions);
            child_process.execSync('git checkout -b 4.8-RC',execOptions);
            child_process.execSync('git checkout -b 4.8',execOptions);
            child_process.execSync('git checkout -b 4.9-DEV',execOptions);
            child_process.execSync('git checkout -b 4.9-RC',execOptions);
            child_process.execSync('git checkout -b 4.9',execOptions);
            child_process.execSync('git checkout -b 4.11-DEV',execOptions);
            child_process.execSync('git checkout -b 4.11-RC',execOptions);
            child_process.execSync('git checkout -b 4.11',execOptions);
            child_process.execSync('git checkout master',execOptions);
        });

        afterEach(() => {
            child_process.execSync('git branch -D 4.8-DEV',execOptions);
            child_process.execSync('git branch -D 4.8-RC',execOptions);
            child_process.execSync('git branch -D 4.8',execOptions);
            child_process.execSync('git branch -D 4.9-DEV',execOptions);
            child_process.execSync('git branch -D 4.9-RC',execOptions);
            child_process.execSync('git branch -D 4.9',execOptions);
            child_process.execSync('git branch -D 4.11-DEV',execOptions);
            child_process.execSync('git branch -D 4.11-RC',execOptions);
            child_process.execSync('git branch -D 4.11',execOptions);
        })
        
        test('não deve permitir commits em branches 4.8 sem commit na 4.8-RC  ', async () => {
            child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
            child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
            const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
            
            const branchesNotPermmitFixes = ['4.8'];
            branchesNotPermmitFixes.forEach(branch => {
                try{
                    const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                        env: {... process.env,
                            GL_PROJECT_PATH: 'plataforma-w/companyw',
                            GL_USERNAME: 'thaise.oliveira'
                        },
                        input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                    }).toString();   
                    
                    expect('in a sucess will note execute this line').toBe('not executed'); 
                } catch (err) {
                    expect(err.stdout.toString()).toMatch(/O commit não existe na branch 4.8-RC/);
                }
                
            })
        });
    
        test('não deve permitir commits em branches 4.8-RC sem commit na 4.8-DEV  ', async () => {
            child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
            child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
            const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
            
            const branchesNotPermmitFixes = ['4.8-RC'];
            branchesNotPermmitFixes.forEach(branch => {
                try{
                    const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                        env: {... process.env,
                            GL_PROJECT_PATH: 'plataforma-w/companyw',
                            GL_USERNAME: 'thaise.oliveira'
                        },
                        input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                    }).toString();   
                    expect('in a sucess will note execute this line').toBe('not executed'); 
                } catch (err) {
                    expect(err.stdout.toString()).toMatch(/O commit não existe na branch 4.8-DEV/);
                }
                
            })
        });
        
        
        
        test('deve permitir commits em branches 4.8-DEV ', async () => {
            child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
            child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
            const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
            
            const branchesNotPermmitFixes = ['4.8-DEV'];
            branchesNotPermmitFixes.forEach(branch => {
                const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                    env: {... process.env,
                        GL_PROJECT_PATH: 'plataforma-w/companyw',
                        GL_USERNAME: 'someuser'
                    },
                    input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                }).toString();   
    
                expect(stdout).toMatch(/Push successful/);  
    
                
            })
        });
    
        test('não deve permitir commits em branches 4.9 sem commit na 4.9-RC  ', async () => {
            child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
            child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
            const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
            
            const branchesNotPermmitFixes = ['4.9'];
            branchesNotPermmitFixes.forEach(branch => {
                try {
                    const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                        env: {... process.env,
                            GL_PROJECT_PATH: 'plataforma-w/companyw',
                            GL_USERNAME: 'thaise.oliveira'
                        },
                        input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                    }).toString();   
                    expect('in a sucess will note execute this line').toBe('not executed');
                } catch (err) {
                    expect(err.stdout.toString()).toMatch(/O commit não existe na branch 4.9-RC/);
                }
    
                
            })
        });
    
        test('não deve permitir commits em branches 4.9-RC sem commit na 4.9-DEV  ', async () => {
            child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
            child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
            const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
            
            const branchesNotPermmitFixes = ['4.9-RC'];
            branchesNotPermmitFixes.forEach(branch => {
                try{
                    const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                        env: {... process.env,
                            GL_PROJECT_PATH: 'plataforma-w/companyw',
                            GL_USERNAME: 'thaise.oliveira'
                        },
                        input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                    }).toString();   
        
                    expect('in a sucess will note execute this line').toBe('not executed');
                } catch (err) {
                    expect(err.stdout.toString()).toMatch(/O commit não existe na branch 4.9-DEV/);
                }
            })
        });

        test('não deve permitir commits em branches 4.9-RC sem commit na 4.9-DEV no projeto company-js ', async () => {
            child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
            child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
            const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
            
            const branchesNotPermmitFixes = ['4.9-RC','4.11-RC'];
            branchesNotPermmitFixes.forEach(branch => {
                try{
                    const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                        env: {... process.env,
                            GL_PROJECT_PATH: 'plataforma-w/company-js',
                            GL_USERNAME: 'thaise.oliveira'
                        },
                        input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                    }).toString();   
        
                    expect('in a sucess will note execute this line').toBe('not executed');
                } catch (err) {
                    expect(err.stdout.toString()).toMatch(/O commit não existe na branch/);
                }
            })
        });

        test('não deve permitir commits com bugedChar no projeto company-js ', async () => {
            child_process.execSync(`echo "©" >> teste.txt`,execOptions).toString();
            child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
            const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
            
            const branchesNotPermmitFixes = ['4.9-DEV'];
            branchesNotPermmitFixes.forEach(branch => {
                try{
                    const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                        env: {... process.env,
                            GL_PROJECT_PATH: 'plataforma-w/company-js',
                            GL_USERNAME: 'thaise.oliveira'
                        },
                        input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                    }).toString();   
        
                    expect('in a sucess will note execute this line').toBe('not executed');
                } catch (err) {
                    expect(err.stdout.toString()).toMatch(/Caracteres inválidos encontrados no arquivo/);
                }
            })
        });
        
        test('deve permitir commits em branches 4.9-DEV  ', async () => {
            child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
            child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();
            const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
            
            const branchesNotPermmitFixes = ['4.9-DEV'];
            branchesNotPermmitFixes.forEach(branch => {
                try {
                    const stdout = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                        env: {... process.env,
                            GL_PROJECT_PATH: 'plataforma-w/companyw',
                            GL_USERNAME: 'thaise.oliveira'
                        },
                        input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
                    }).toString();   
            
                    expect(stdout).toMatch(/Push successful./);  
                    
                } catch (err) {
                    console.log('-----------------------------------------------------------------');
                    console.log(err.stdout.toString());
                    console.log('-----------------------------------------------------------------');
                    console.log(err);
                    console.log('-----------------------------------------------------------------');
                    
                    expect(err).toMatch(/Push successful./);  

                }
    
                
            })
        });
    });


})
