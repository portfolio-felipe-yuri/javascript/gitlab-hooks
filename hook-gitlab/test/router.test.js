var child_process = require('child_process');
const {prepareBeforeAll} = require('./utils')

beforeAll(() => {
    prepareBeforeAll()

})

test('Deve dar exit 0 com branch criação de branch local', async () => {

    const stdout = child_process.execSync('node src/pre-receive.js', {
        env: {... process.env,
            GL_PROJECT_PATH: 'root/teste'
        },
        input: '0000000000000000000000000000000000000000 926709e5e2bd33b2be388e285fe0c7bdcf366c04 refs/heads/minha-nova-branch'
    }).toString();
    expect(stdout).toMatch(/Push successful/);
});