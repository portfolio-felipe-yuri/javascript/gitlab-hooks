var child_process = require('child_process');

test('Deve dar exit 1 quando não há env file', async () => {
    const execOptions = {}
    execOptions.cwd = '/tmp'
    process.env.testCWD= '/tmp'
    execOptions.env = process.env

    try {
        const ret = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, execOptions)               
        expect('Not execute this line').toBe('');
    } catch (err) {
        expect(err.stdout.toString()).toBe('GL-HOOK-ERR: Falha ao carregar váriaveis de ambiente, contate a equipe de devops\n')
    }

});