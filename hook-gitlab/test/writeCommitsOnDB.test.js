var child_process = require('child_process');
const knex = require('knex');
require('dotenv').config({path: '/data/hook-gitlab/.env'});
const autoservicecrud = require('@js-skeleton/autoservicecrud');
const {prepareBeforeAll} = require('./utils')

const execOptions = {
    cwd: '/tmp/gitmock'
}

let ultimocommitos;
let commitos;
let commitosFontes;
let dbsrcmodify;
let dbsrcmodifyCustom
const app = {}

beforeAll(() => {
    prepareBeforeAll()

    app.db = knex({
        client: 'mysql2',
        connection: {
          host: process.env.DB_HOST || 'dbdownloads.cibvajzgcbpg.sa-east-1.rds.amazonaws.com',
          user: process.env.DB_USER || 'hookgitlab-test',
          password: process.env.DB_PASSWORD || 'ZA6XtcvXgwahpjyg',
          database: process.env.DB_DATABASE || 'hookgitlab-test',  
        },
      });
    
    ultimocommitos = autoservicecrud(app, 'ultimocommitos');
    commitos = autoservicecrud(app, 'commitos');
    commitosFontes = autoservicecrud(app, 'commitos_fontes');
    dbsrcmodify = autoservicecrud(app, 'dbsrcmodify');
    dbsrcmodifyCustom = autoservicecrud(app, 'dbsrcmodify_erp_pessoal');

})

afterAll(() => {
    app.db.destroy();
    child_process.execSync(`rm -Rf ./gitmock-*`).toString();
})

beforeEach(async () => {
    await ultimocommitos.del({NUMOS: 1697591})

    execOptions.cwd = `${execOptions.cwd}-${Date.now()}`

    child_process.execSync(`mkdir -p ${execOptions.cwd}`).toString();
    child_process.execSync('git init',execOptions).toString();
    child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
    child_process.execSync('git add . && git commit -m "OS: 999999 teste"',execOptions).toString();
    firstCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
});

afterEach(async () => {
    execOptions.cwd = '/tmp/gitmock'
});

const makeDummyCommit = () => {
    child_process.execSync(`echo "teste" >> teste.txt`,execOptions).toString();
    child_process.execSync(`git add . && git commit -m "OS: 1697591 teste"`,execOptions).toString();
}

const spreadToRcBranches = (branch) => {
    try {
        child_process.execSync(`git branch -D ${branch}-DEV`,execOptions).toString();
        child_process.execSync(`git branch -D ${branch}-RC`,execOptions).toString();
    } catch (err) {
        
    }
    
    child_process.execSync(`git checkout -b ${branch}-DEV`,execOptions).toString();
    child_process.execSync(`git checkout -b ${branch}-RC`,execOptions).toString();


}

test('Deve gravar os dados na tabela ultimocommitos', async () => {
    makeDummyCommit();

    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    
    const branchesNotPermmitFixes = ['4.9','4.8'];
    branchesNotPermmitFixes.forEach(branch => {
        
        spreadToRcBranches(branch);
        
        try {
            child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw',
                    GL_USERNAME: 'thaise.oliveira'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString();               
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const ultimosCommits = await ultimocommitos.select({NUMOS: '1697591'})

    
    expect(ultimosCommits.length).toBe(2);
    
});

test('Deve gravar os dados na tabela ultimocommitos estando no bypassAll', async () => {
    makeDummyCommit();

    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    
    const branchesNotPermmitFixes = ['4.9','4.8'];
    branchesNotPermmitFixes.forEach(branch => {
        spreadToRcBranches(branch);
        try {
            child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw',
                    GL_USERNAME: 'gitlab.devops'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString();               
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const ultimosCommits = await ultimocommitos.select({NUMOS: '1697591'})

    
    expect(ultimosCommits.length).toBe(2);
    
});

test('Não Deve gravar os dados na tabela ultimocommitos estando fora do IsARepoToHook', async () => {
    makeDummyCommit();

    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    
    const branchesNotPermmitFixes = ['4.9','4.8'];
    branchesNotPermmitFixes.forEach(branch => {
         
        try {
            child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/xurupita',
                    GL_USERNAME: 'gitlab.devops'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString();               
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const ultimosCommits = await ultimocommitos.select({NUMOS: '1697591'})

    
    expect(ultimosCommits.length).toBe(0);
    
});

test('Deve gravar os dados na tabela DBSCRIPT', async () => {
    child_process.execSync(`mkdir dbscript`,execOptions).toString();
    child_process.execSync(`echo "teste" >> dbscript/teste.xsql`,execOptions).toString();
    child_process.execSync(`echo "teste" >> dbscript/teste2.xsql`,execOptions).toString();
    child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();

    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    
    const branchesNotPermmitFixes = ['4.10.3','4.9','4.10'];

    branchesNotPermmitFixes.forEach(branch => {
        
        spreadToRcBranches(branch);
        
        try {
            const ret = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw',
                    GL_USERNAME: 'thaise.oliveira'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString(); 
            console.log(ret);             
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const dbsrc = await dbsrcmodify.select({path: 'dbscript/teste.xsql'})    
    const dbsrc2 = await dbsrcmodify.select({path: 'dbscript/teste2.xsql'})    
    await dbsrcmodify.del({path: 'dbscript/teste.xsql'})    
    await dbsrcmodify.del({path: 'dbscript/teste2.xsql'})    
    expect(dbsrc.length).toBe(3);
    expect(dbsrc2.length).toBe(3);
    
});

test('Deve gravar os dados na tabela DBSCRIPT com bypassAll', async () => {
    child_process.execSync(`mkdir dbscript`,execOptions).toString();
    child_process.execSync(`echo "teste" >> dbscript/teste.xsql`,execOptions).toString();
    child_process.execSync(`echo "teste" >> dbscript/teste2.xsql`,execOptions).toString();
    child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();

    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    
    const branchesNotPermmitFixes = ['4.10.3','4.9','4.10'];
    branchesNotPermmitFixes.forEach(branch => {
        spreadToRcBranches(branch)

              
        try {
            const ret = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw',
                    GL_USERNAME: 'gitlab.devops'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString(); 
            console.log(ret);             
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const dbsrc = await dbsrcmodify.select({path: 'dbscript/teste.xsql'})    
    const dbsrc2 = await dbsrcmodify.select({path: 'dbscript/teste2.xsql'})    
    await dbsrcmodify.del({path: 'dbscript/teste.xsql'})    
    await dbsrcmodify.del({path: 'dbscript/teste2.xsql'})    
    expect(dbsrc.length).toBe(3);
    expect(dbsrc2.length).toBe(3);
    
});

test('Deve gravar os dados na tabela DBSCRIPT custom com bypassAll', async () => {
    child_process.execSync(`mkdir dbscript`,execOptions).toString();
    child_process.execSync(`echo "teste" >> dbscript/teste.xsql`,execOptions).toString();
    child_process.execSync(`echo "teste" >> dbscript/teste2.xsql`,execOptions).toString();
    child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();

    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    
    const branchesNotPermmitFixes = ['4.10.3','4.9','4.10'];
    branchesNotPermmitFixes.forEach(branch => {
        spreadToRcBranches(branch)

              
        try {
            const ret = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'erp/pessoal/mge-pes-dbscript',
                    GL_USERNAME: 'gitlab.devops'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString(); 
            console.log(ret);             
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const dbsrc = await dbsrcmodifyCustom.select({path: 'dbscript/teste.xsql'})    
    const dbsrc2 = await dbsrcmodifyCustom.select({path: 'dbscript/teste2.xsql'})    
    await dbsrcmodifyCustom.del({path: 'dbscript/teste.xsql'})    
    await dbsrcmodifyCustom.del({path: 'dbscript/teste2.xsql'})    
    expect(dbsrc.length).toBe(3);
    expect(dbsrc2.length).toBe(3);
    
});

test('Não deve gravar os dados na tabela DBSCRIPT não estando no IsARepoToHook', async () => {
    child_process.execSync(`mkdir dbscript`,execOptions).toString();
    child_process.execSync(`echo "teste" >> dbscript/teste.xsql`,execOptions).toString();
    child_process.execSync(`echo "teste" >> dbscript/teste2.xsql`,execOptions).toString();
    child_process.execSync('git add . && git commit -m "OS: 1697591 teste"',execOptions).toString();

    const actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    
    const branchesNotPermmitFixes = ['4.10.3','4.9','4.10'];
    branchesNotPermmitFixes.forEach(branch => {
              
        try {
            const ret = child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/xurupita',
                    GL_USERNAME: 'gitlab.devops'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString(); 
            console.log(ret);             
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const dbsrc = await dbsrcmodify.select({path: 'dbscript/teste.xsql'})    
    const dbsrc2 = await dbsrcmodify.select({path: 'dbscript/teste2.xsql'})    
    await dbsrcmodify.del({path: 'dbscript/teste.xsql'})    
    await dbsrcmodify.del({path: 'dbscript/teste2.xsql'})    
    expect(dbsrc.length).toBe(0);
    expect(dbsrc2.length).toBe(0);
    
});

test('Deve atualizar um ultimoCommitos já existente', async () => {
    makeDummyCommit();

    let actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    
    const branchesNotPermmitFixes = ['4.9','4.8'];
    branchesNotPermmitFixes.forEach(branch => {
        
        spreadToRcBranches(branch);
        
        try {
            child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw',
                    GL_USERNAME: 'thaise.oliveira'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString();               
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const ultimosCommits = await ultimocommitos.select({NUMOS: '1697591'})
    expect(ultimosCommits.length).toBe(2);

    firstCommitHash = actualCommitHash;

    makeDummyCommit();
    makeDummyCommit();

    actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();

    branchesNotPermmitFixes.forEach(branch => {
        
        spreadToRcBranches(branch);
        
        try {
            child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw',
                    GL_USERNAME: 'thaise.oliveira'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString();               
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const newUltimosCommits = await ultimocommitos.select({NUMOS: '1697591'})
    expect(ultimosCommits.length).toBe(2);

    for(let i = ultimocommitos.length -1; i>=0; i--){
        expect(newUltimosCommits.DATA).toBeGreaterThan(ultimocommitos.DATA)
    }


},20000)

test('Deve atualizar um ultimoCommitos já existente estando no bypassAll', async () => {
    makeDummyCommit();
    let actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();
    
    const branchesNotPermmitFixes = ['4.11','4.12'];
    branchesNotPermmitFixes.forEach(branch => {
        spreadToRcBranches(branch)
               
        try {
            child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw',
                    GL_USERNAME: 'gitlab.devops'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString();               
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const ultimosCommits = await ultimocommitos.select({NUMOS: '1697591'})
    expect(ultimosCommits.length).toBe(2);

    firstCommitHash = actualCommitHash;

    makeDummyCommit();
    makeDummyCommit();

    actualCommitHash = child_process.execSync('git rev-parse HEAD',execOptions).toString();

    branchesNotPermmitFixes.forEach(branch => {
        spreadToRcBranches(branch)
        try {
            child_process.execSync(`node ${process.cwd()}/src/pre-receive.js`, {... execOptions,
                env: {... process.env,
                    GL_PROJECT_PATH: 'plataforma-w/companyw',
                    GL_USERNAME: 'gitlab.devops'
                },
                input: `${firstCommitHash} ${actualCommitHash} refs/heads/${branch}`.replace(/\n/g,'')
            }).toString();               
        } catch (err) {
            console.log(err.stdout.toString())
            throw err
        }
        
        
        
    })
    
    const newUltimosCommits = await ultimocommitos.select({NUMOS: '1697591'})
    expect(ultimosCommits.length).toBe(2);

    for(let i = ultimocommitos.length -1; i>=0; i--){
        expect(newUltimosCommits.DATA).toBeGreaterThan(ultimocommitos.DATA)
    }


},20000)